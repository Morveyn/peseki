function RandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min)) + min;
}

class Dog {
    name;
    hp = 10;
    hunger = 10;

    constructor(newName) {
        this.name = newName;
    }

    eat(food) {
        for (let i = this.hunger; i > 0; i--) {
            if (food.calories >= 1) {
                food.calories -= 1;
                this.hunger -= 1;
            }
        }
    }

    lifecycle(){
        this.hunger++;
        if(this.hunger >= 8){
            this.hp--;
        }
    }
}

class Food {
    nameOfFood = 'Еда';
    calories;

    constructor() {
        this.calories = RandomInt(1, 5);
    }
}

class BloodHound extends Dog {
    conura = [];

    searchFood() {
        this.conura.push(new Food());
    }

    lifecycle() {

        if (this.hunger > 0) {
            if (this.conura.length != 0) {
                this.conura = this.conura.filter((element) => { 
                    (element != undefined && element != NaN) ? this.eat(element) : slice(element, 1), element--;
                }) 
            }
            else {
                this.searchFood();
                console.log('Я ищу пищу из-за 50 строки');
            }
        }
        super.lifecycle();
    }
}

class Psina extends Dog {
    punishFood(bloodHound) {
        if (this.hunger > 0) {
            if (bloodHound.conura.length != 0) {
                bloodHound.conura = bloodHound.conura.filter((element) => {
                    if(element != undefined && element != NaN){
                        this.eat(element);
                        (element.calories == 0) ? bloodHound.conura.slice(element, 1) : false;
                    }
                    else{
                        slice(element, 1);
                        element--;
                    }  
                }) 

            }
        }
    }

    lifecycle(mass) {
        mass = mass.filter((element) => {       
            (element instanceof BloodHound) ? this.punishFood(element) : false;   
        }) 
        super.lifecycle();
    }
}

let street = [new BloodHound('Sharik'), new BloodHound('Dryjok'), new Psina('Byaka')];

for (let life = 0; life < 20; life++) {
    
    this.street = street.filter((element) => {
        console.log(element);
        (element instanceof BloodHound) ? element.lifecycle() : element.lifecycle(street);
        console.log(element);
    })
    
    this.street = street.filter((element) => {       
        (element.hp == 0) ? street.slice(element, 1) : false;   
    })
}

street = street.filter((element) => {
    console.log(element);
})